package org.flab.tracker.domain.enumeration;

/**
 * The Units enumeration.
 */
public enum Units {
    kg,lb
}
