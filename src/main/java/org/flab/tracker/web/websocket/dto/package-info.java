/**
 * Data Access Objects used by WebSocket services.
 */
package org.flab.tracker.web.websocket.dto;
