/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package org.flab.tracker.web.rest.dto;
