'use strict';

angular.module('trackerApp')
    .factory('BloodPressureSearch', function ($resource) {
        return $resource('api/_search/bloodPressures/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
