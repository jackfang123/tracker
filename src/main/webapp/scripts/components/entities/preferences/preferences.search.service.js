'use strict';

angular.module('trackerApp')
    .factory('PreferencesSearch', function ($resource) {
        return $resource('api/_search/preferencess/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
